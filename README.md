# 实证金融课程主页


&emsp;

## 0. &#x1F34E; 作业发布
- [【点击查看作业】](https://gitee.com/arlionn/EFLN/wikis/%E4%BD%9C%E4%B8%9A%E8%A6%81%E6%B1%82.md?sort_id=2861320)  (或直接点击上方 [Wiki](https://gitee.com/arlionn/EFLN/wikis/Home) 按钮查看。）
  - 第一次作业 (**HW01**) 已经发布，提交截止时间：`2020/9/18`
  - ${\color{red}{New}}$ &#x1F449;  第二次作业已经发布，截止时间 `2020/9/21`

### &#x1F353; 交作业入口
> &#x1F449; **【[点击提交](https://workspace.jianguoyun.com/inbox/collect/fd1c32c79a6c4780aa58c6752302dd96/submit)】**，个人作业和期末课程报告均通过此处提交 

> Notes：  
>  [1] 若需更新，只需将同名文件再次提交即可。   
>  [2] 提交截止时间为 deadline 当日 23:59，逾期不候。   
   
&emsp; 

## 1. 课程概览

- **连玉君**，<arlionn@163.com>，[主页](https://www.lianxh.cn)，[知乎](https://www.zhihu.com/people/arlionn/)，[码云](https://gitee.com/arlionn)
  - **Office Hour：** 周一 16:00-18:00，行政中心 412。
  - **Note：** 请务必想清楚你的问题是什么，最好能写出来，整理成一个文档，否则我不予作答。
- **课程主页：**<https://gitee.com/arlionn/EFLN>，发布作业、期末课程报告要求等
- 参考资料：
  - [计量书籍](https://quqi.com/s/880197/hmpmu2ylAcvHnXwY)，亦可到我的 FTP 上下载
  - [连玉君的链接](https://www.lianxh.cn/news/9e917d856a654.html) 提供了论文复现链接
  - [论文重现网站大全](https://www.lianxh.cn/news/e87e5976686d5.html)

&emsp;


## 2. 课件下载和使用

参见 PDF 讲义

&emsp;

## 3. 专题介绍

- **T1**. 实证研究设计与论文写作
- **T2**. 线性回归与模型设定
- **T3**. 静态面板数据模型
- **T4**. 动态面板数据模型
- **T5**. 倍分法 (DID)
- **T6**. Bootstrap 和 Monte Carlo 模拟分析
- **T7**. 断点回归分析 (RDD)
- **T8**. 合成控制法 (SCM)
- **T9**. 论文解读：Flannery and Rangan (2006, JFE)

&emsp;



![](https://fig-lianxh.oss-cn-shenzhen.aliyuncs.com/横条-远山03-窄版.jpg)

&emsp;